$(document).ready(function() {
    $('.secmenu_trigger.open').on('click', function(e) {
        e.preventDefault();
        $('body').addClass('secmenu_active');
    });
    $('.secmenu_trigger.mobile').on('click', function(e) {
        e.preventDefault();
        $('body').addClass('secmenu_active');
    });
    $('.secmenu_trigger.close').on('click', function(e) {
        e.preventDefault();
        $('body').removeClass('secmenu_active');
    });
    $('.scroll_slider').slick({
        arrows: false,
        infinite: false,
        speed: 700,
        slidesToShow: 1,
        autoplay : false,
        pauseOnHover : false,
        slidesToScroll : 1,
        dots: true,
    });
    $('.gallery_slider').slick({
        arrows: true,
        infinite: false,
        speed: 700,
        slidesToShow: 1,
        autoplay : false,
        pauseOnHover : false,
        slidesToScroll : 1,
        dots: true,
        dotsClass: 'custom_paging',
        customPaging: function (slider, i) {
            console.log(slider);
            return  (i + 1) + '/' + slider.slideCount;
        }
    });
    $('.top_notibar p i').on('click', function(e) {
        e.preventDefault();
        $('.top_notibar').remove();
    });
    var distance = $('.home_sec').offset().top+180,
    $window = $(window);
    if ( $window.scrollTop() >= distance ) {
        $('header .logo').addClass('active');
    } else {
        $('header .logo').removeClass('active');
    }
    $window.scroll(function() {
        if ( $window.scrollTop() >= distance ) {
            $('header .logo').addClass('active');
        } else {
            $('header .logo').removeClass('active');
        }
    });
    $('.main_menu__sec li a[href*="#"]:not([href="#"])').click(function(e){
        e.preventDefault();
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 600);
    });
    $('.secmobile_menu li a[href*="#"]:not([href="#"])').click(function(e){
        e.preventDefault();
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 600);
    });
    $('footer .social_item a[href*="#"]:not([href="#"])').click(function(e){
        e.preventDefault();
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 600);
    });
});
$( window ).on("load", function() {
    $('body').addClass('page_loaded');
    if($('.top_notibar')[0]) {
        setInterval(function(){ 
            $('.top_notibar').addClass('active');
        }, 2000);
    }
    $('#cta-submit').click(function() {
        $('#page_title').text("Installation instructions!");
        $('#instructions').text("Go into your Android settings and enable your browser to install unknown apps. Then open the APK file you just downloaded to install!")
        $('#instruction-gif').show();
    });
});

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}