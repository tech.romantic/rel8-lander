<?php
$api_key = 'b4ae4f77d6f776ef01b1eadb65a465fb-us4';
$list_id = 'c6fa46df41';
include('./MailChimp_fun.php');
use \DrewM\MailChimp\MailChimp;
$MailChimp = new MailChimp($api_key);

$merge_values = [
    "FNAME" => $_POST['fname']
];
//'merge_fields'  => ['FNAME'=>$_POST["fname"], 'LNAME'=>$_POST["lname"]],
$result = $MailChimp->post("lists/$list_id/members", [
    'email_address' => $_POST["email"],
    'merge_fields' 	=> $merge_values,
    'status'        => 'subscribed',
]);

if ($MailChimp->success()) {
    echo '<div class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" role="alert">Thanks, you are Subscribed!</div>';
} else {
	$msg = $MailChimp->getLastError();
	if(strpos($msg, 'already') !== false){
		echo '<div class="wpcf7-response-output wpcf7-display-none wpcf7-validation-errors" role="alert" style="display: block;">Whoops, this email is already subscribed.</div>';
	} else{
		echo '<div class="wpcf7-response-output wpcf7-display-none wpcf7-validation-errors" role="alert" style="display: block;">Whoops, try again.</div>';
	}
}